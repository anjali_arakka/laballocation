import java.util.Scanner;

public class CheckMagicSquare {

    public static void main(String[] arg) {

        Scanner sc = new Scanner(System.in) ;
        int size = sc.nextInt() ;

        int[][] matrix = new int[size][size] ;


        for (int i=0; i<size; i++) {
            for (int j = 0; j < size; j++) {
                matrix[i][j] = sc.nextInt();
            }
        }

        boolean flag = true ;

//        For checking Diagonals
        int diagonalSum1 = 0, diagonalSum2 = 0 ;
        for (int i=0; i<size; i++){

            diagonalSum1 += matrix[i][i] ;
            diagonalSum2 += matrix[i][size-1-i] ;

        }
        if(diagonalSum1 != diagonalSum2){
            flag = false ;
        }

        //        For checking Rows Columns
        for (int i=0; i<size; i++) {
            int rowSum = 0, columnSum = 0;;
            for (int j = 0; j < size; j++) {

                rowSum += matrix[i][j];
                columnSum += matrix[j][i];

            }
            if( (rowSum != columnSum) || (rowSum != diagonalSum1) || (rowSum != diagonalSum2) ){
                flag = false ;
            }
        }

        if(flag) {
            System.out.println("yes");
        }
        else {
            System.out.println("no");
        }

    }

}
