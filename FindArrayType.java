import java.util.Scanner;

public class FindArrayType {


    public static int identifyType(int[] array){

        int evenCount=0, oddCount=0 ;
        for(int j=0; j<array.length; j++) {

            if (array[j] % 2 == 0) {
                evenCount++;
            } else {
                oddCount++;
            }
        }

            if (evenCount == array.length) {
                return 1 ;
            }
            else if(oddCount == array.length){
                return 2;
            }
            else {
                return 3 ;
            }

    }

    public static void main(String[] arg) {

        Scanner sc =  new Scanner(System.in) ;
        int arraySize = sc.nextInt() ;
        int[] array = new int [arraySize] ;

        for(int i=0; i<arraySize; i++) {
           array[i] = sc.nextInt() ;
//           scan.nextInt() scanner.nextInt()
        }

        int result = identifyType(array) ;

        switch (result) {

            case 1:
                System.out.println("Even");
                break ;

            case 2:
                System.out.println("Odd");
                break ;

            case 3:
                System.out.println("Mixed");
                break ;
        }





    }

}
