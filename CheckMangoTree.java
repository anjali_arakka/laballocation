import java.util.Scanner;

public class CheckMangoTree {

    public static void main(String[] arg) {

        Scanner sc = new Scanner(System.in) ;
        int rows = sc.nextInt() ;
        int columns = sc.nextInt() ;
        int treeNumber = sc.nextInt();

        int[][] rectangle = new int[rows][columns] ;


        if( (treeNumber>=1 && treeNumber<=columns) || (treeNumber%columns == 0) || (treeNumber%columns == 1) || treeNumber<=(rows*columns) ) {
            System.out.println("yes");
        }
        else {
            System.out.println("no");
        }

    }
}
