import java.util.Arrays;
import java.util.Scanner;



public class FindArrayStatics {

    public static double meanFind(int[] array) {

        int sum = 0 ;
        for (int i = 0; i < array.length; i++) {
            sum += array[i] ;
        }
        return (double)sum/(double)array.length ;



    }

    public static double medianFind(int[] array) {

        Arrays.sort(array);

        double median ;

        if(array.length%2 != 0){
            median = array[array.length/2] ;
        }
        else {
            median = (array[array.length/2] + array[(array.length/2) -1])/2.0 ;
        }

        return median;

    }

    public static double modeFind(int[] array) {

        int size = (int) 1e7;
        int[] frequency = new int[size] ;

        for (int i = 0; i < size; i++) {
            frequency[i] = 0 ;
        }

        for (int i = 0; i < array.length; i++) {
            frequency[array[i]]++ ;
        }

        int maxValue = frequency[0] ;
        double mode = 0 ;

        for (int i = 0; i < size; i++) {
            if(frequency[i] > maxValue){
                maxValue = frequency[i] ;
                mode = i ;
            }
        }

        return mode ;

    }


    public static void main(String[] arg) {


        Scanner sc = new Scanner(System.in);
        int arraySize = sc.nextInt();
        int[] array = new int[arraySize];

        for (int i = 0; i < arraySize; i++) {
            array[i] = sc.nextInt();
        }

        double meanValue = meanFind(array) ;
        System.out.printf("%,2f", meanValue) ;

        double medianValue = medianFind(array);
        System.out.printf("%,2f", medianValue) ;


        double modeValue = modeFind(array) ;
        System.out.printf("%,2f", modeValue) ;

    }
}

