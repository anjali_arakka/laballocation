import java.util.Scanner;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class FindArrayRange {

    public static void main(String[] arg) {


        Scanner sc = new Scanner(System.in) ;
        int arraySize = sc.nextInt() ;
        int[] array = new int[arraySize];

        for (int i = 0; i < arraySize; i++) {
            array[i] = sc.nextInt();
        }

        int maxElement = Integer.MIN_VALUE;
        int minElement = Integer.MAX_VALUE ;

        for (int j=0; j<arraySize; j++) {
            maxElement = max(maxElement, array[j]) ;
            minElement = min(minElement, array[j]) ;
        }
        int range = maxElement - minElement ;

        System.out.println(range);

    }
}

